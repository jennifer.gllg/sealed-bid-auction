/**
 * @author Jennifer G.
 */
public class Test {

    public static void main(String[] args) {

        Buyer buyerA = new Buyer("A", new int[]{110, 130});
        Buyer buyerB = new Buyer("B", new int[0]);
        Buyer buyerC = new Buyer("C", new int[]{125});
        Buyer buyerD = new Buyer("D", new int[]{105, 115, 90});
        Buyer buyerE = new Buyer("E", new int[]{132, 135, 140});

        Buyer[] buyers = new Buyer[5];
        buyers[0] = buyerA;
        buyers[1] = buyerB;
        buyers[2] = buyerC;
        buyers[3] = buyerD;
        buyers[4] = buyerE;

        Auction auction = new Auction(100, buyers);

        auction.getBestBidByBuyer();
        auction.winner = auction.getWinner();
        auction.winningPrice = auction.getWinningPrice();

        if (auction.winningPrice < auction.reservePrice) {
            System.out.println("No bids have reached the reserve price.");
        }
        else {
            System.out.println(String.format("The buyer %s wins the auction at the price of %s euros.",
                    auction.winner.getId(),
                    auction.winningPrice));
        }
    }
}
