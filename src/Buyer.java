/**
 * @author Jennifer G.
 */
public class Buyer {
    private String id;
    public int[] bids;
    public int bestBid;

    Buyer(String id, int[] bids) {
        this.id = id;
        this.bids = bids;
    }

    public String getId() {
        return id;
    }
}
