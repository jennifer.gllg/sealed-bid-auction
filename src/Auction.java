import java.util.Arrays;

/**
 * @author Jennifer G.
 */
public class Auction {
    private Buyer[] buyers;
    public int reservePrice;
    public int winningPrice;
    public Buyer winner;

    Auction(int reservePrice, Buyer[] buyers) {
        this.reservePrice = reservePrice;
        this.buyers = buyers;
    }

    /**
     * Returns buyer with the best bid
     * @return Buyer winning the auction
     */
    public Buyer getWinner() {
        int auctionBestBid = 0;

        for (Buyer buyer : this.buyers) {
            if (buyer.bestBid > auctionBestBid)
            {
                auctionBestBid = buyer.bestBid;
                this.winner = buyer;
            }
        }
        return winner;
    }

    /**
     * Returns second higher bid
     * @return auction's selling price
     */
    public int getWinningPrice() {
        for (Buyer buyer : this.buyers) {
            if (this.winner != null &&
                    buyer.getId() != this.winner.getId() &&
                    buyer.bestBid > this.winningPrice) {
                this.winningPrice = buyer.bestBid;
            }
        }
        return this.winningPrice;
    }

    /**
     * Calculates each buyer's best bid
     */
    public void getBestBidByBuyer() {
        Arrays.stream(this.buyers).forEach((Buyer buyer) -> {
            if (buyer.bids.length != 0) {
                int bestBid = Arrays.stream(buyer.bids).max().getAsInt();
                if (bestBid > this.reservePrice) {
                    buyer.bestBid = bestBid;
                }
            }
        });
    }
}
